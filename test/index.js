'use strict';
var chai = require('chai');
chai.config.includeStack = true;
chai.use(require('chai-as-promised'));

var expect = chai.expect;
//var AssertionError = chai.AssertionError;
//var Assertion = chai.Assertion;
//var assert = chai.assert;

var main = require('../lib/index');

describe('stocktwits get symbol streams', function () {
  it('get nasdaq symbols', function() {
    var promise = main.symbolDownloadService.getSymbols("nasdaq");
    return expect( promise ).to.eventually.be.fulfilled;
  });
});

describe('stocktwits get symbol streams', function () {
  it('get nasdaq symbols', function() {
    var promise = main.stockFeedDownloadService.getEodQuote("MSFT", "nasdaq");
    return expect( promise ).to.eventually.have.property("quote");
  });
});
