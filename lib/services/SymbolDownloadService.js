var request = require('request');
var Company = require('../models/Company');
var Promise = require('promise');
var _ = require('lodash');

var SymbolDownloadService = {
  getSymbols: function (market) {
    return downloadSymbols(market);
  }
};

function downloadSymbols(market) {
  var url = "http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=" + market + "&render=download";
  return new Promise(function (resolve, reject) {
    request.get(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {

        resolve(parseCsvResponse(body, market));
      } else {
        reject({
          error: error
        });
      }
    });
  });
}

function parseCsvResponse(body, market) {
  var header = [];
  var headerRead = false;

  var result = [];
  var contentArray = body.split(",\r\n");

  _.each(contentArray, function (content) {

    var values = content.split("\",\"");
    values = _.map(values, function (value) {
      return value.replace("\"", "");
    });

    if (!headerRead) {
      header = values;
      headerRead = true;
    } else {
      console.log("processing " + values);
      if (values.length !== header.length) {
        console.log("error:" + values);
      } else {
        var company = new Company(header, values, market);
        result.push(company);
      }
    }
  });

  return result;
}

module.exports = SymbolDownloadService;

