var yqlUtil = require("../utils/YqlUtils");
var S = require("string");
var Promise = require('promise');
var Quote = require('../models/Quote');

var YAHOO_STOCK_QUOTE_YQL = "select * from yahoo.finance.quotes where symbol in ('{{symbol}}')";

var service = {
  getEodQuote: function (symbol, market) {
    var values = {symbol: symbol};
    var yqlSource = S(YAHOO_STOCK_QUOTE_YQL).template(values).s;
    return new Promise(function (resolve, reject) {
      yqlUtil.executeQuery(yqlSource, function (error, response) {
        if (!error && response.query) {
          var resultResponse = response.query.results;
          if (resultResponse) {
            var quoteResponse = resultResponse.quote;
            if (quoteResponse && quoteResponse.Change) {
              resolve(new Quote(quoteResponse, market));
            }
          }
        }
        reject({
          error : error,
          message: "cannot get quote for " + symbol
        });
      });
    });
  }
};

module.exports = service;
