var Company = function (propertyList, valueList, market) {
    var self = this;
    for (var i = 0; i < propertyList.length; i++) {
        var property = propertyList[i];
        var value = valueList[i];
        if (property === 'Symbol') {
            self.symbol = value;
        } else if (property === 'Name') {
            self.name = value;
        } else if (property === 'LastSale') {
            self.lastSale = parseFloat(value);
        } else if (property === 'MarketCap') {
            self.marketCap = value;
        } else if (property === 'IPOyear') {
            if(isNaN(value)) {
                value = 0;
            }
            self.ipoYear = parseInt(value);
        } else if (property === 'Sector') {
            self.sector = value;
        } else if (property === 'industry') {
            self.industry = value;
        }
    }
    self.market = market;

    return self;
};


module.exports = Company;
