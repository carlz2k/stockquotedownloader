var Utils= require('../utils/Utils');
var Quote = function (data, market) {
    var self = this;
    self.symbol = data.symbol;
    self.market = market;
    self.close = Utils.toNumeric(data.LastTradePriceOnly);
    self.open = Utils.toNumeric(data.Open);
    self.volume = Math.round(Utils.toNumeric(data.Volume));
    self.dayHigh = Utils.toNumeric(data.DaysHigh);
    self.dayLow = Utils.toNumeric(data.DaysLow);
    self.yearLow = Utils.toNumeric(data.YearLow);
    self.yearHigh = Utils.toNumeric(data.YearHigh);
    self.date = new Date(data.LastTradeDate);
    self.dividendYield = Utils.toNumeric(data.DividendYield);
    self.earningPerShare=Utils.toNumeric(data.EarningsShare);
    self.priceEarningRatio=Utils.toNumeric(data.PERatio);
    self.marketCap = data.MarketCapitalization;
    self.averageVolume = Utils.toNumeric(data.AverageDailyVolume);
    self.type='equity';
    return self;
};



module.exports = Quote;
