
function createService() {
    var service = {};
    service.toNumeric = function(stringValue) {
        if (stringValue && !isNaN(stringValue)) {
            return parseFloat(stringValue);
        } else {
            return 0;
        }
    };
    return service;
}

module.exports = createService();
