var YQL = require('yql');
var S = require("string");

function createService() {
    var services = {};

    services.executeQuery = function (url, callbacks) {
        var query = new YQL(url);
        query.exec(function (error, response) {
            callbacks(error, response);
        });
    };

    services.getHtmlContentYqlQuery = function(url) {
        var HTML_SELECT_YQL_QUERY="select * from html where url='{{url}}' ";
        return S(HTML_SELECT_YQL_QUERY).template({url:url}).s;
    };
    return services;
}

module.exports = createService();

