# stockquotedownloader [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> stock quote downloader from yahoo


## Install

```sh
$ npm install --save stockquotedownloader
```


## Usage

```js
var stockquotedownloader = require('stockquotedownloader');

stockquotedownloader('Rainbow');
```

## License

ISC © [carlz2k]()


[npm-image]: https://badge.fury.io/js/stockquotedownloader.svg
[npm-url]: https://npmjs.org/package/stockquotedownloader
[travis-image]: https://travis-ci.org//stockquotedownloader.svg?branch=master
[travis-url]: https://travis-ci.org//stockquotedownloader
[daviddm-image]: https://david-dm.org//stockquotedownloader.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//stockquotedownloader
